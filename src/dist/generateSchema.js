class GenerateSchema {

  constructor(sequelize) {
    this.models = sequelize.models
    this.structureModel = {}
    this.typesDef = ``
    this.typesQueries = `type Query`
  }

  mkTypeDef() {
    Object.keys(this.models).forEach(model => {
      let tableName = this.models[model].getTableName()
      let nameTypeDef = tableName.slice(0, 1).toUpperCase() + tableName.slice(1).toLowerCase();
      this.models[model].describe().then(structure => {
        for (let atribute in structure) {
          this.structureModel[atribute] = this.getType(structure[atribute].type)
        }
        this.typesDef += 'type ' + nameTypeDef + ' ' + this.formatText(JSON.stringify(this.structureModel))
        this.typesQueries += 'type Query {\n'

      })
    })
    console.log()
  }
  //TO DO: Add more types
  getType(sequelizeType) {
    sequelizeType = sequelizeType.split('(')[0]
    if (['VARCHAR', 'CHAR', 'TEXT', 'DATETIME', 'DATE'].indexOf(sequelizeType) >= 0) {
      return 'String'
    } else if (['BIGINT', 'INTEGER', 'SMALLINT'].indexOf(sequelizeType) >= 0) {
      return 'Int'
    }
  }

  formatText(text) {
    return text.replace(/['"]+/g, '').replace(/,/g, ',\n')
  }

}

export default GenerateSchema