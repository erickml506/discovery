import Apollo from './server/apollo/apollo'
import app from './server/express/express'


const server = Apollo.getServer()
server.applyMiddleware({ app })

app.listen({ port: process.env.API_PORT }, () =>
  console.log(`🚀 Server ready at http://localhost:${process.env.API_PORT}${server.graphqlPath}`)
) 