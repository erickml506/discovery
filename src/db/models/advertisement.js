module.exports = function(sequelize, DataTypes) {
  return sequelize.define('advertisement', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(80),
      allowNull: false
    },
    adType: {
      type: DataTypes.ENUM('bg','main_banner','top_left','top_right','sidebar','footer','mobile_splash','landing_banner','gift_cards_banner','gift_cards_modal'),
      allowNull: false
    },
    timeZoneId: {
      type: DataTypes.STRING(50),
      allowNull: false,
      references: {
        model: 'time_zone',
        key: 'id'
      }
    },
    countryId: {
      type: DataTypes.STRING(2),
      allowNull: true,
      references: {
        model: 'country',
        key: 'id'
      }
    },
    cityId: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'city',
        key: 'id'
      }
    },
    movieId: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'movie',
        key: 'id'
      }
    },
    movieTheaterChainId: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'movie_theater_chain',
        key: 'id'
      }
    },
    movieTheaterId: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'movie_theater',
        key: 'id'
      }
    },
    contentImage: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    contentIframe: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    fromDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    toDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    fromDateServer: {
      type: DataTypes.DATE,
      allowNull: true
    },
    toDateServer: {
      type: DataTypes.DATE,
      allowNull: true
    },
    targetUrl: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    targetType: {
      type: DataTypes.ENUM('blank','self'),
      allowNull: true
    },
    viewsCountTotal: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '0'
    },
    viewsCountLimit: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '0'
    },
    position: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '100'
    },
    visible: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: '0'
    },
    appliesTo: {
      type: DataTypes.ENUM('web','mobile','kiosks','mobile_ios','mobile_android','mobile_win','mobile_firefox'),
      allowNull: false
    },
    appId: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    actionName: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    frequency: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    created_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    }
  }, {
    tableName: 'advertisement',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
