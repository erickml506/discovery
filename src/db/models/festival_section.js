module.exports = function(sequelize, DataTypes) {
  return sequelize.define('festival_section', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    festivalId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'festival',
        key: 'id'
      }
    },
    name: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    sectionType: {
      type: DataTypes.ENUM('showing','contest'),
      allowNull: false
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    position: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '100'
    },
    visible: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: '1'
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    created_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    }
  }, {
    tableName: 'festival_section',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
