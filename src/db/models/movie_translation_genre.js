module.exports = function(sequelize, DataTypes) {
  return sequelize.define('movie_translation_genre', {
    movieTranslationId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'movie_translation',
        key: 'id'
      }
    },
    genreId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'genre',
        key: 'id'
      }
    }
  }, {
    tableName: 'movie_translation_genre',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
