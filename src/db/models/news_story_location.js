module.exports = function(sequelize, DataTypes) {
  return sequelize.define('news_story_location', {
    newsStoryId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'news_story',
        key: 'id'
      }
    },
    countryId: {
      type: DataTypes.STRING(2),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'country',
        key: 'id'
      }
    },
    publishDate: {
      type: DataTypes.DATE,
      allowNull: false
    },
    publishDateServer: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    tableName: 'news_story_location',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
