module.exports = function(sequelize, DataTypes) {
  return sequelize.define('exhibit_language', {
    id: {
      type: DataTypes.STRING(60),
      allowNull: false,
      primaryKey: true
    },
    label: {
      type: DataTypes.STRING(60),
      allowNull: true
    },
    shortText: {
      type: DataTypes.STRING(60),
      allowNull: true
    },
    active: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    created_by: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_by: {
      type: DataTypes.STRING(50),
      allowNull: false
    }
  }, {
    tableName: 'exhibit_language',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
