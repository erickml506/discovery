module.exports = function(sequelize, DataTypes) {
  return sequelize.define('movie', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    nameSlug: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    emsId: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    nameSpanish: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    nameOriginal: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    nameEnglish: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    synopsis: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    originalName: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    studioId: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'studio',
        key: 'id'
      }
    },
    distributorId: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'distributor',
        key: 'id'
      }
    },
    motionPictureRatingId: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'motion_picture_rating',
        key: 'id'
      }
    },
    posterImage: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    trailerVideo: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    jwplayerVideo: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    backgroundImage: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    extraInfo: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    backgroundMainImage: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    movieUrl: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    originalCountry: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    originalReleaseYear: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    originalReleaseMonth: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    durationMinutes: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    criticRating: {
      type: "DOUBLE",
      allowNull: true
    },
    keywords: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    position: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '1000'
    },
    redirectToMovieId: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    visible: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: '1'
    },
    oldUniqueId: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    approvedBy: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    created_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    }
  }, {
    tableName: 'movie',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
