module.exports = function(sequelize, DataTypes) {
  return sequelize.define('discovery_banner_promotion', {
    bannerId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'discovery_banner',
        key: 'id'
      }
    },
    promotionId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'discovery_promotion',
        key: 'id'
      }
    }
  }, {
    tableName: 'discovery_banner_promotion',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
