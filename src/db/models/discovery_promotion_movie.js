module.exports = function(sequelize, DataTypes) {
  return sequelize.define('discovery_promotion_movie', {
    promotionId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'discovery_promotion',
        key: 'id'
      }
    },
    movieId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'movie',
        key: 'id'
      }
    }
  }, {
    tableName: 'discovery_promotion_movie',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
