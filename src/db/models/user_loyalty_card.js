module.exports = function(sequelize, DataTypes) {
  return sequelize.define('user_loyalty_card', {
    userId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'user',
        key: 'id'
      }
    },
    movieTheaterChainId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'movie_theater_chain',
        key: 'id'
      }
    },
    loyaltyNumber: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    created_by: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_by: {
      type: DataTypes.STRING(50),
      allowNull: false
    }
  }, {
    tableName: 'user_loyalty_card',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
