module.exports = function(sequelize, DataTypes) {
  return sequelize.define('selling_message', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    messageType: {
      type: DataTypes.ENUM('info','warning','error'),
      allowNull: false
    },
    messageToDisplay: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    active: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: '0'
    },
    position: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '100'
    },
    movieTheaterChainId: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'movie_theater_chain',
        key: 'id'
      }
    },
    movieTheaterId: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'movie_theater',
        key: 'id'
      }
    },
    movieTimeIds: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    screenNumbers: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    movieIds: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    created_by: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_by: {
      type: DataTypes.STRING(50),
      allowNull: false
    }
  }, {
    tableName: 'selling_message',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
