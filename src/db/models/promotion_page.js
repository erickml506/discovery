module.exports = function(sequelize, DataTypes) {
  return sequelize.define('promotion_page', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(80),
      allowNull: false
    },
    nameSlug: {
      type: DataTypes.STRING(80),
      allowNull: false
    },
    timeZoneId: {
      type: DataTypes.STRING(60),
      allowNull: false,
      references: {
        model: 'time_zone',
        key: 'id'
      }
    },
    appliesToCountry: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: '1'
    },
    countryId: {
      type: DataTypes.STRING(2),
      allowNull: true,
      references: {
        model: 'country',
        key: 'id'
      }
    },
    movieTheaterId: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    cityId: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'city',
        key: 'id'
      }
    },
    movieTheaterChainId: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    contentImage: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    targetUrl: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    targetType: {
      type: DataTypes.ENUM('blank','self'),
      allowNull: true
    },
    fromDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    toDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    fromDateServer: {
      type: DataTypes.DATE,
      allowNull: true
    },
    toDateServer: {
      type: DataTypes.DATE,
      allowNull: true
    },
    visible: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: '0'
    },
    showInMenu: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: '0'
    },
    contentHtml: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    created_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    }
  }, {
    tableName: 'promotion_page',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
