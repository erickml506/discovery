module.exports = function(sequelize, DataTypes) {
  return sequelize.define('festival', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    nameSlug: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    displayName: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: '1'
    },
    timeZoneId: {
      type: DataTypes.STRING(60),
      allowNull: false,
      references: {
        model: 'time_zone',
        key: 'id'
      }
    },
    festivalDate: {
      type: DataTypes.DATE,
      allowNull: false
    },
    festivalDateServer: {
      type: DataTypes.DATE,
      allowNull: false
    },
    bannerImage: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    logoImage: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    bgColorHex: {
      type: DataTypes.STRING(7),
      allowNull: true,
      defaultValue: '#FFFFFF'
    },
    textColorHex: {
      type: DataTypes.STRING(7),
      allowNull: true,
      defaultValue: '#000000'
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    created_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    }
  }, {
    tableName: 'festival',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
