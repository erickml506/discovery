module.exports = function(sequelize, DataTypes) {
  return sequelize.define('movie_genre', {
    movieId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'movie',
        key: 'id'
      }
    },
    genreId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'genre',
        key: 'id'
      }
    }
  }, {
    tableName: 'movie_genre',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
