module.exports = function(sequelize, DataTypes) {
  return sequelize.define('promotion_prices_invalid_dates', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    promotionPriceId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'promotion_price',
        key: 'id'
      }
    },
    invalidDate: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    active: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: '1'
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    created_by: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_by: {
      type: DataTypes.STRING(50),
      allowNull: false
    }
  }, {
    tableName: 'promotion_prices_invalid_dates',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
