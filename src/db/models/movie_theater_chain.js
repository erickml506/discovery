module.exports = function(sequelize, DataTypes) {
  return sequelize.define('movie_theater_chain', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    nameSlug: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    fullName: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    countryId: {
      type: DataTypes.STRING(2),
      allowNull: false,
      references: {
        model: 'country',
        key: 'id'
      }
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    address: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    phone: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    email: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    taxPayerNumber: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    photoImage: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    iconImage: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    invoiceImage: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    mapLocatorImage: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    position: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '100'
    },
    visible: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: '1'
    },
    secretToken: {
      type: DataTypes.STRING(32),
      allowNull: false
    },
    loyaltyCardActive: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: '0'
    },
    loyaltyCardName: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    loyaltyCardValidator: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    timeFormat: {
      type: DataTypes.ENUM('24h','12h'),
      allowNull: false,
      defaultValue: '12h'
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    created_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    oldIdTheater: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    oldIdCountry: {
      type: DataTypes.STRING(2),
      allowNull: true
    },
    oldUri: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    tableName: 'movie_theater_chain',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
