module.exports = function(sequelize, DataTypes) {
  return sequelize.define('combo', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    description: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    active: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: '1'
    },
    multiplier: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '1'
    },
    quantity: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    reserved: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    price: {
      type: "DOUBLE",
      allowNull: false
    },
    photoImage: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    is_promotion: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: '0'
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    created_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    }
  }, {
    tableName: 'combo',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
