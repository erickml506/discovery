module.exports = function(sequelize, DataTypes) {
  return sequelize.define('movie_theater_provider', {
    movieTheaterId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'movie_theater',
        key: 'id'
      }
    },
    className: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    prodJson: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    devJson: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    identifier: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    ticketCorrelativePurchase: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: '1'
    },
    ticketCorrelativeVoid: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: '1'
    },
    ticketTemplate: {
      type: DataTypes.STRING(50),
      allowNull: false,
      defaultValue: 'default'
    },
    ticketHtml: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    apiEndpoint: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    apiDataJson: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    alertEmailsJson: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    invoicingModule: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    created_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    }
  }, {
    tableName: 'movie_theater_provider',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
