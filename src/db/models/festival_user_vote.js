module.exports = function(sequelize, DataTypes) {
  return sequelize.define('festival_user_vote', {
    userId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    festivalSectionCategoryId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'festival_section_category',
        key: 'id'
      }
    },
    personOrMovieId: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    countryId: {
      type: DataTypes.STRING(2),
      allowNull: true,
      references: {
        model: 'country',
        key: 'id'
      }
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    created_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    }
  }, {
    tableName: 'festival_user_vote',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
