module.exports = function(sequelize, DataTypes) {
  return sequelize.define('movie_time_promotion_price', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    movieId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'movie',
        key: 'id'
      }
    },
    movieTheaterChainId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'movie_theater_chain',
        key: 'id'
      }
    },
    movieTheaterId: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'movie_theater',
        key: 'id'
      }
    },
    priceType: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    priceKeyword: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    unique: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: '0'
    },
    fromDate: {
      type: DataTypes.DATE,
      allowNull: false
    },
    toDate: {
      type: DataTypes.DATE,
      allowNull: false
    },
    active: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: '1'
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    created_by: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_by: {
      type: DataTypes.STRING(50),
      allowNull: false
    }
  }, {
    tableName: 'movie_time_promotion_price',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
