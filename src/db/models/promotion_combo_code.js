module.exports = function(sequelize, DataTypes) {
  return sequelize.define('promotion_combo_code', {
    promotionId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'promotion_combo',
        key: 'id'
      }
    },
    code: {
      type: DataTypes.STRING(50),
      allowNull: false,
      primaryKey: true
    },
    quantity: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    used: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: '0'
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    created_by: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_by: {
      type: DataTypes.STRING(50),
      allowNull: false
    }
  }, {
    tableName: 'promotion_combo_code',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
