module.exports = function(sequelize, DataTypes) {
  return sequelize.define('movie_theater_chain_payment_processor', {
    movieTheaterChainId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'movie_theater_chain',
        key: 'id'
      }
    },
    paymentProcessorId: {
      type: DataTypes.STRING(10),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'payment_processor',
        key: 'id'
      }
    },
    position: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    active: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: '1'
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    created_by: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_by: {
      type: DataTypes.STRING(50),
      allowNull: false
    }
  }, {
    tableName: 'movie_theater_chain_payment_processor',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
