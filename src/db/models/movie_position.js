module.exports = function(sequelize, DataTypes) {
  return sequelize.define('movie_position', {
    countryId: {
      type: DataTypes.STRING(2),
      allowNull: false,
      primaryKey: true
    },
    movieId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'movie',
        key: 'id'
      }
    },
    position: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    created_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    }
  }, {
    tableName: 'movie_position',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
