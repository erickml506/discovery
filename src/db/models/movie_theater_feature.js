module.exports = function(sequelize, DataTypes) {
  return sequelize.define('movie_theater_feature', {
    movieTheaterId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'movie_theater',
        key: 'id'
      }
    },
    featureId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'feature',
        key: 'id'
      }
    }
  }, {
    tableName: 'movie_theater_feature',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
