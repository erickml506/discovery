module.exports = function(sequelize, DataTypes) {
  return sequelize.define('promotion_combo', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    promoName: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    comboId: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    comboType: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    comboKeyword: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    unique: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    fromDate: {
      type: DataTypes.DATE,
      allowNull: false
    },
    toDate: {
      type: DataTypes.DATE,
      allowNull: false
    },
    maxCombos: {
      type: DataTypes.INTEGER(2),
      allowNull: true
    },
    discount: {
      type: "DOUBLE",
      allowNull: true
    },
    quantity: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: '0'
    },
    maxQuantity: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    active: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    created_by: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_by: {
      type: DataTypes.STRING(50),
      allowNull: false
    }
  }, {
    tableName: 'promotion_combo',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
