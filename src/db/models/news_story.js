module.exports = function(sequelize, DataTypes) {
  return sequelize.define('news_story', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    title: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    titleSlug: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    metaTitle: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    shortDescription: {
      type: DataTypes.STRING(2000),
      allowNull: true
    },
    mainImage: {
      type: DataTypes.STRING(2000),
      allowNull: false
    },
    content: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    infographicHtml: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    contentPlain: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    movieId: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'movie',
        key: 'id'
      }
    },
    editorId: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'editor',
        key: 'id'
      }
    },
    editorName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    editorUrl: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    visible: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: '0'
    },
    isMainArticle: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: '1'
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    created_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    oldIdArticle: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    oldIdCountry: {
      type: DataTypes.STRING(2),
      allowNull: true
    },
    oldUri: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    oldUniqueId: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    version: {
      type: DataTypes.INTEGER(4),
      allowNull: true,
      defaultValue: '1'
    },
    contentHtml: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    tableName: 'news_story',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
