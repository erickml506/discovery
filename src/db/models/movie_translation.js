module.exports = function(sequelize, DataTypes) {
  return sequelize.define('movie_translation', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    movieId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'movie',
        key: 'id'
      }
    },
    languageId: {
      type: DataTypes.STRING(2),
      allowNull: false,
      references: {
        model: 'language',
        key: 'id'
      }
    },
    countryId: {
      type: DataTypes.STRING(2),
      allowNull: false,
      references: {
        model: 'country',
        key: 'id'
      }
    },
    movieTheaterChainId: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'movie_theater_chain',
        key: 'id'
      }
    },
    movieTheaterId: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'movie_theater',
        key: 'id'
      }
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    synopsis: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    distributorId: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'distributor',
        key: 'id'
      }
    },
    motionPictureRatingId: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'motion_picture_rating',
        key: 'id'
      }
    },
    posterImage: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    trailerVideo: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    jwplayerVideo: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    backgroundImage: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    extraInfo: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    originalCountry: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    criticRating: {
      type: "DOUBLE",
      allowNull: true
    },
    position: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '100'
    },
    visible: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: '1'
    },
    oldIdMovie: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    oldUniqueId: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    oldUriMovie: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    created_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    }
  }, {
    tableName: 'movie_translation',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
