module.exports = function(sequelize, DataTypes) {
  return sequelize.define('movie_theater', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    movieTheaterChainId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'movie_theater_chain',
        key: 'id'
      }
    },
    movieTheaterChainGroupId: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    cityId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'city',
        key: 'id'
      }
    },
    currencyId: {
      type: DataTypes.STRING(3),
      allowNull: false,
      references: {
        model: 'currency',
        key: 'id'
      }
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    nameSlug: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    fullName: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    latitude: {
      type: "DOUBLE",
      allowNull: false
    },
    longitude: {
      type: "DOUBLE",
      allowNull: false
    },
    mapZoom: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '14'
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    address: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    phone: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    email: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    taxPayerNumber: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    photoImage: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    iconImage: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    pricesText: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    pricesHtml: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    ticketingTheater: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: '0'
    },
    sellingEnabled: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: '0'
    },
    sellingDisabledMessage: {
      type: DataTypes.STRING(200),
      allowNull: false,
      defaultValue: 'Las ventas están suspendidas temporalmente en este local'
    },
    position: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '100'
    },
    visible: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: '1'
    },
    maxPrice: {
      type: "DOUBLE",
      allowNull: true
    },
    minPrice: {
      type: "DOUBLE",
      allowNull: true
    },
    averagePrice: {
      type: "DOUBLE",
      allowNull: true
    },
    handicap: {
      type: DataTypes.ENUM('yes','no','na','full'),
      allowNull: false,
      defaultValue: 'na'
    },
    parking: {
      type: DataTypes.ENUM('free','paid','none','na'),
      allowNull: false,
      defaultValue: 'na'
    },
    promos: {
      type: DataTypes.ENUM('yes','no','na'),
      allowNull: false,
      defaultValue: 'na'
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    created_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    oldIdTheater: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    oldIdCountry: {
      type: DataTypes.STRING(2),
      allowNull: true
    },
    oldUri: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    tableName: 'movie_theater',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
