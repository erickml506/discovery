module.exports = function(sequelize, DataTypes) {
  return sequelize.define('user_admin_movie_theater', {
    userAdminId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'user_admin',
        key: 'id'
      }
    },
    movieTheaterId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'movie_theater',
        key: 'id'
      }
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    created_by: {
      type: DataTypes.STRING(20),
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_by: {
      type: DataTypes.STRING(20),
      allowNull: false
    }
  }, {
    tableName: 'user_admin_movie_theater',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
