module.exports = function(sequelize, DataTypes) {
  return sequelize.define('discovery_promotion_theater', {
    promotionId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'discovery_promotion',
        key: 'id'
      }
    },
    movieTheaterId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'movie_theater',
        key: 'id'
      }
    }
  }, {
    tableName: 'discovery_promotion_theater',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
