module.exports = function(sequelize, DataTypes) {
  return sequelize.define('freelance_editor', {
    userId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'user_admin',
        key: 'id'
      }
    },
    languages: {
      type: DataTypes.STRING(100),
      allowNull: true
    }
  }, {
    tableName: 'freelance_editor',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
