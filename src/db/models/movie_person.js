module.exports = function(sequelize, DataTypes) {
  return sequelize.define('movie_person', {
    movieId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'movie',
        key: 'id'
      }
    },
    personId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'person',
        key: 'id'
      }
    },
    role: {
      type: DataTypes.ENUM('actor','director','voice','crew'),
      allowNull: false,
      primaryKey: true
    },
    characterName: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    created_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    }
  }, {
    tableName: 'movie_person',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
