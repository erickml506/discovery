module.exports = function(sequelize, DataTypes) {
  return sequelize.define('movie_theater_combo', {
    movieTheaterId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'movie_theater',
        key: 'id'
      }
    },
    comboId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'combo',
        key: 'id'
      }
    },
    price: {
      type: "DOUBLE",
      allowNull: true
    },
    active: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: '1'
    },
    order: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '100'
    }
  }, {
    tableName: 'movie_theater_combo',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
