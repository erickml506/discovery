module.exports = function(sequelize, DataTypes) {
  return sequelize.define('person_translation', {
    personId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'person',
        key: 'id'
      }
    },
    languageId: {
      type: DataTypes.STRING(2),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'language',
        key: 'id'
      }
    },
    countryId: {
      type: DataTypes.STRING(2),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'country',
        key: 'id'
      }
    },
    biographyHtml: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    birthPlace: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    zodiac: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    weight: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    height: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    eyeColor: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    hairColor: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    maritalStatus: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    created_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    }
  }, {
    tableName: 'person_translation',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
