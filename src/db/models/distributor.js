module.exports = function(sequelize, DataTypes) {
  return sequelize.define('distributor', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    nameSlug: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    descriptionHtml: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    homePageUrl: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    mainImage: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    contactEmail: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    phone: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    visible: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: '1'
    },
    oldCountryId: {
      type: DataTypes.STRING(2),
      allowNull: true
    },
    oldDistributorId: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    created_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    }
  }, {
    tableName: 'distributor',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
