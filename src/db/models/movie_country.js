module.exports = function(sequelize, DataTypes) {
  return sequelize.define('movie_country', {
    movieId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'movie',
        key: 'id'
      }
    },
    countryId: {
      type: DataTypes.STRING(2),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'country',
        key: 'id'
      }
    }
  }, {
    tableName: 'movie_country',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
