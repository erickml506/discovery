module.exports = function(sequelize, DataTypes) {
  return sequelize.define('news_story_video', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    newsStoryId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'news_story',
        key: 'id'
      }
    },
    sourceVideo: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    legalInfo: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    legend: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    created_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    }
  }, {
    tableName: 'news_story_video',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
