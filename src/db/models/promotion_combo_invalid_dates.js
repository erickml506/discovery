module.exports = function(sequelize, DataTypes) {
  return sequelize.define('promotion_combo_invalid_dates', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    promotionId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'promotion_combo',
        key: 'id'
      }
    },
    invalidDate: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    active: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: '1'
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    created_by: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_by: {
      type: DataTypes.STRING(50),
      allowNull: false
    }
  }, {
    tableName: 'promotion_combo_invalid_dates',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
