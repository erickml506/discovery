module.exports = function(sequelize, DataTypes) {
  return sequelize.define('movie_position_city_chain', {
    cityId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'city',
        key: 'id'
      }
    },
    movieTheaterChainId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'movie_theater_chain',
        key: 'id'
      }
    },
    publishDate: {
      type: DataTypes.DATEONLY,
      allowNull: false,
      primaryKey: true
    },
    movieIds: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    created_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    }
  }, {
    tableName: 'movie_position_city_chain',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
