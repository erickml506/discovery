module.exports = function(sequelize, DataTypes) {
  return sequelize.define('movie_time', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    movieId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'movie',
        key: 'id'
      }
    },
    movieTheaterId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'movie_theater',
        key: 'id'
      }
    },
    exhibitModeId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'exhibit_mode',
        key: 'id'
      }
    },
    screenNumber: {
      type: DataTypes.STRING(40),
      allowNull: true
    },
    timeZoneId: {
      type: DataTypes.STRING(60),
      allowNull: false,
      references: {
        model: 'time_zone',
        key: 'id'
      }
    },
    exhibitLanguage: {
      type: DataTypes.STRING(60),
      allowNull: false
    },
    exhibitionDate: {
      type: DataTypes.DATE,
      allowNull: false
    },
    exhibitionDateServer: {
      type: DataTypes.DATE,
      allowNull: false
    },
    providerDate: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    providerTime: {
      type: DataTypes.TIME,
      allowNull: false
    },
    providerId: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    status: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    dataJson: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    tableName: 'movie_time',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
