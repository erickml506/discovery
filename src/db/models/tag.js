module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tag', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    position: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '100'
    },
    title: {
      type: DataTypes.STRING(100),
      allowNull: false,
      unique: true
    },
    titleSlug: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    meta_title: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    meta_description: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    created_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    }
  }, {
    tableName: 'tag',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
