module.exports = function(sequelize, DataTypes) {
  return sequelize.define('editor', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    mainImage: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    gender: {
      type: DataTypes.ENUM('M','F','U'),
      allowNull: false,
      defaultValue: 'U'
    },
    socialNetwork: {
      type: DataTypes.ENUM('facebook','twitter','googleplus'),
      allowNull: true
    },
    socialId: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    created_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    }
  }, {
    tableName: 'editor',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
