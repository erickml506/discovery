module.exports = function(sequelize, DataTypes) {
  return sequelize.define('festival_location', {
    festivalId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'festival',
        key: 'id'
      }
    },
    countryId: {
      type: DataTypes.STRING(2),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'country',
        key: 'id'
      }
    },
    userVotingEnabled: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: '0'
    },
    metaDescription: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    userVotingDescriptionHtml: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    userVotingIntroHtml: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    userVotingAfterHtml: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    userVotingTermsHtml: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    created_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    }
  }, {
    tableName: 'festival_location',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
