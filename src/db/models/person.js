module.exports = function(sequelize, DataTypes) {
  return sequelize.define('person', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    nameSlug: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    headShotImage: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    gender: {
      type: DataTypes.ENUM('M','F','U'),
      allowNull: false,
      defaultValue: 'U'
    },
    visible: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: '1'
    },
    emsId: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    realName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    biographyHtml: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    citation: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    birthDate: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    birthPlace: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    zodiac: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    weight: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    height: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    eyeColor: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    hairColor: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    maritalStatus: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    oldUniqueId: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    oldSlug: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    approvedBy: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    created_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    }
  }, {
    tableName: 'person',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
