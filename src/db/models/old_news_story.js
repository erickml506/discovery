module.exports = function(sequelize, DataTypes) {
  return sequelize.define('old_news_story', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    title: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    titleSlug: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    shortDescription: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    mainImage: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    contentHtml: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    editorName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    editorUrl: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    visible: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: '0'
    },
    created_at: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    created_by: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    updated_at: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    updated_by: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    newId: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    oldIdArticle: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    oldIdCountry: {
      type: DataTypes.STRING(2),
      allowNull: true
    },
    oldUri: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    oldUniqueId: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    oldPublishDate: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    oldMainArticle: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: '0'
    }
  }, {
    tableName: 'old_news_story',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
