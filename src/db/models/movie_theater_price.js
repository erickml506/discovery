module.exports = function(sequelize, DataTypes) {
  return sequelize.define('movie_theater_price', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    priceTypeId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'price_type',
        key: 'id'
      }
    },
    movieTheaterId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'movie_theater',
        key: 'id'
      }
    },
    exhibitModeId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'exhibit_mode',
        key: 'id'
      }
    },
    price: {
      type: "DOUBLE",
      allowNull: false
    },
    fromWeekDay: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    fromTime: {
      type: DataTypes.TIME,
      allowNull: true
    },
    toWeekDay: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    toTime: {
      type: DataTypes.TIME,
      allowNull: true
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    created_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    }
  }, {
    tableName: 'movie_theater_price',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
