module.exports = function(sequelize, DataTypes) {
  return sequelize.define('movie_release', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    movieId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'movie',
        key: 'id'
      }
    },
    countryId: {
      type: DataTypes.STRING(2),
      allowNull: false,
      references: {
        model: 'country',
        key: 'id'
      }
    },
    movieTheaterId: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'movie_theater',
        key: 'id'
      }
    },
    fromDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    fromDateServer: {
      type: DataTypes.DATE,
      allowNull: true
    },
    toDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    toDateServer: {
      type: DataTypes.DATE,
      allowNull: true
    },
    fromDateUpTo: {
      type: DataTypes.ENUM('YEAR','MONTH','DAY'),
      allowNull: false,
      defaultValue: 'DAY'
    },
    visible: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: '1'
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    created_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_by: {
      type: DataTypes.STRING(45),
      allowNull: false
    }
  }, {
    tableName: 'movie_release',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
