module.exports = function(sequelize, DataTypes) {
  return sequelize.define('movie_theater_chain_fee', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    movieTheaterChainId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'movie_theater_chain',
        key: 'id'
      }
    },
    currencyId: {
      type: DataTypes.STRING(3),
      allowNull: false,
      references: {
        model: 'currency',
        key: 'id'
      }
    },
    restClientId: {
      type: DataTypes.STRING(32),
      allowNull: true,
      references: {
        model: 'rest_client',
        key: 'id'
      }
    },
    seatFeePercent: {
      type: "DOUBLE",
      allowNull: false,
      defaultValue: '5'
    },
    seatFeeMin: {
      type: "DOUBLE",
      allowNull: false,
      defaultValue: '1'
    },
    seatFeeMax: {
      type: "DOUBLE",
      allowNull: false,
      defaultValue: '99999999.99'
    },
    comboFeePercent: {
      type: "DOUBLE",
      allowNull: false,
      defaultValue: '5'
    },
    comboFeeMin: {
      type: "DOUBLE",
      allowNull: false,
      defaultValue: '1'
    },
    comboFeeMax: {
      type: "DOUBLE",
      allowNull: false,
      defaultValue: '99999999.99'
    },
    souvenirFeePercent: {
      type: "DOUBLE",
      allowNull: false,
      defaultValue: '5'
    },
    souvenirFeeMin: {
      type: "DOUBLE",
      allowNull: false,
      defaultValue: '1'
    },
    souvenirFeeMax: {
      type: "DOUBLE",
      allowNull: false,
      defaultValue: '99999999.99'
    },
    seatOuterFeePercent: {
      type: "DOUBLE",
      allowNull: false,
      defaultValue: '0'
    },
    seatOuterFeeMin: {
      type: "DOUBLE",
      allowNull: false,
      defaultValue: '0'
    },
    seatOuterFeeMax: {
      type: "DOUBLE",
      allowNull: false,
      defaultValue: '99999999.99'
    },
    comboOuterFeePercent: {
      type: "DOUBLE",
      allowNull: false,
      defaultValue: '0'
    },
    comboOuterFeeMin: {
      type: "DOUBLE",
      allowNull: false,
      defaultValue: '0'
    },
    comboOuterFeeMax: {
      type: "DOUBLE",
      allowNull: false,
      defaultValue: '99999999.99'
    },
    souvenirOuterFeePercent: {
      type: "DOUBLE",
      allowNull: false,
      defaultValue: '0'
    },
    souvenirOuterFeeMin: {
      type: "DOUBLE",
      allowNull: false,
      defaultValue: '0'
    },
    souvenirOuterFeeMax: {
      type: "DOUBLE",
      allowNull: false,
      defaultValue: '99999999.99'
    },
    ccTheaterVariable: {
      type: "DOUBLE",
      allowNull: false,
      defaultValue: '40'
    },
    ccTheaterFixed: {
      type: "DOUBLE",
      allowNull: false,
      defaultValue: '0'
    },
    ccVariable: {
      type: "DOUBLE",
      allowNull: false,
      defaultValue: '3.7'
    },
    ccFixed: {
      type: "DOUBLE",
      allowNull: false,
      defaultValue: '0.9'
    },
    isByTransaction: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: '0'
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    created_by: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_by: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    comboFeeByTransaction: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: '0'
    }
  }, {
    tableName: 'movie_theater_chain_fee',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
