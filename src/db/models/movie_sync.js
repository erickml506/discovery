module.exports = function(sequelize, DataTypes) {
  return sequelize.define('movie_sync', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    movieId: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'movie',
        key: 'id'
      }
    },
    providerId: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    movieProviderId: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    movieProviderName: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    status: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    exhibitModeId: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'exhibit_mode',
        key: 'id'
      }
    },
    exhibitLanguage: {
      type: DataTypes.STRING(60),
      allowNull: true
    }
  }, {
    tableName: 'movie_sync',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
