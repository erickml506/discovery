module.exports = function(sequelize, DataTypes) {
  return sequelize.define('promotion_price', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    priceType: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    priceKeyword: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    unique: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    fromDate: {
      type: DataTypes.DATE,
      allowNull: false
    },
    toDate: {
      type: DataTypes.DATE,
      allowNull: false
    },
    maxTickets: {
      type: DataTypes.INTEGER(2),
      allowNull: true
    },
    discount: {
      type: "DOUBLE",
      allowNull: true
    },
    quantity: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: '0'
    },
    maxQuantity: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    active: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    created_by: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_by: {
      type: DataTypes.STRING(50),
      allowNull: false
    }
  }, {
    tableName: 'promotion_price',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
