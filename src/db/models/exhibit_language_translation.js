module.exports = function(sequelize, DataTypes) {
  return sequelize.define('exhibit_language_translation', {
    exhibitLanguageId: {
      type: DataTypes.STRING(60),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'exhibit_language',
        key: 'id'
      }
    },
    languageId: {
      type: DataTypes.STRING(2),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'language',
        key: 'id'
      }
    },
    label: {
      type: DataTypes.STRING(60),
      allowNull: false
    },
    shortText: {
      type: DataTypes.STRING(60),
      allowNull: true
    },
    active: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: '1'
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    created_by: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_by: {
      type: DataTypes.STRING(50),
      allowNull: false
    }
  }, {
    tableName: 'exhibit_language_translation',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
