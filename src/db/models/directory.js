module.exports = function(sequelize, DataTypes) {
  return sequelize.define('directory', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    theaterChainId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'movie_theater_chain',
        key: 'id'
      }
    },
    theaterId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'movie_theater',
        key: 'id'
      }
    },
    fullName: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    phone: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    email: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    cargo: {
      type: DataTypes.STRING(255),
      allowNull: false
    }
  }, {
    tableName: 'directory',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
