module.exports = function(sequelize, DataTypes) {
  return sequelize.define('combo_movie', {
    comboId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'combo',
        key: 'id'
      }
    },
    movieId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'movie',
        key: 'id'
      }
    },
    filter: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    tableName: 'combo_movie',
    timestamps: false,
    underscored: false,
    paranoid: false
  });
};
