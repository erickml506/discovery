import Sequelize from 'sequelize'
import fs from 'fs'
import path from 'path'

class Database {

  constructor(props) {
    this.db_host = props.DB_HOST
    this.db_name = props.DB_NAME
    this.db_user = props.DB_USER
    this.db_password = props.DB_PASSWORD
    this.db_dialect = props.DB_DIALECT
    this.sequelize = null
  }

  getConnection() {
    this.sequelize = new Sequelize(this.db_name, this.db_user, this.db_password, {
      host: this.db_host,
      dialect: this.db_dialect,
    });
    this.setModels()
    return this.sequelize
  }

  setModels() {
    fs.readdirSync(path.join(__dirname, 'models')).forEach(file => {
      if (file.search('.js') >= 0) {
        this.sequelize.import(path.join(__dirname, 'models') + '/' + file)
      }
    })
  }

}

export default Database
