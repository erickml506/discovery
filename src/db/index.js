import Database from './database'
import dotenv from 'dotenv' 
dotenv.config()

let database = new Database({
    DB_NAME : process.env.DB_NAME,
    DB_HOST : process.env.DB_HOST,
    DB_USER : process.env.DB_USER,
    DB_PASSWORD : process.env.DB_PASSWORD,
    DB_DIALECT : process.env.DB_DIALECT
})

let conexion = database.getConnection()

export default conexion
