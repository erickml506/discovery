import db from '../db'

const resolvers = {
  Query: {
    categories: async () => {
      let categories =  await db.models.category.findAll({ limit: 10 });
      categories = categories.map( (item) => {
        return item.dataValues;
      });
      return categories;
    },
    movies: async () => {
      let movies = await db.models.movie.findAll({ limit: 10 });
      movies = movies.map( (item) => {
        return item.dataValues;
      });
      return movies;
    },
    movieImages: async () => {
      let movieImages = await db.models.movie_image.findAll({ limit: 10 });
      movieImages = movieImages.map( (item) => {
        return item.dataValues;
      });
      return movieImages;
    }
  },
}

export default resolvers