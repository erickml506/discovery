import { ApolloServer } from 'apollo-server-express'
import  typeDefs  from '../../schema/schema'
import resolvers from '../../resolvers/resolvers'

class Apollo {
    static getServer() {
        const server = new ApolloServer({ typeDefs, resolvers });
        return server
    }
}

export default Apollo