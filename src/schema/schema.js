import { gql } from 'apollo-server-express'

const typeDefs = gql`
  type Category {
    id: Int,
    name: String,
    created_at: String,
    created_by: String,
    updated_at: String,
    updated_by: String
  }
  
  type MovieImage {
    id: Int,
    movieId: Int,
    sourceImage: String,
    position: String,
    caption: String,
    assetId: String,
    visible: Boolean
  }

  type Movie {
    id: Int,
    images: [MovieImage],
    name: String,
    nameSlug: String,
    emsId: String,
    nameSpanish: String,
    nameOriginal: String,
    nameEnglish: String,
    synopsis: String,
    originalName: String,
    studioId: String,
    distributorId: String,
    motionPictureRatingId: String,
    posterImage: String,
    trailerVideo: String,
    jwplayerVideo: String,
    backgroundImage: String,
    extraInfo: String,
    backgroundMainImage: String,
    movieUrl: String,
    originalCountry: String,
    originalReleaseYear: Int,
    originalReleaseMonth: Int,
    durationMinutes: Int,
    criticRating: Float,
    keywords: String,
    position: Int,
    redirectToMovieId: Int,
    visible: Boolean,
    oldUniqueid: String,
    approveBy: String,
    created_at: String,
    created_by: String,
    updated_at: String,
    updated_by: String,
  }

  type Query {
    categories: [Category],
    movies: [Movie],
    movieImages: [MovieImage]
  }
`;
export default typeDefs
