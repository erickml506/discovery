const path = require('path');
const nodeExternals = require('webpack-node-externals');
const Dotenv = require('dotenv-webpack');

module.exports = {
    mode : process.env.NODE_ENV || 'development',
    target: 'node',
    entry: './src/index.js',
    output: {
        filename: 'index.js',
        path: path.resolve(__dirname, '')
    },
    plugins: [
        new Dotenv()
    ],
    externals: [nodeExternals()]
};